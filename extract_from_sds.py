import requests as r
import json

f = open("./data/name.json", "r")
names = json.loads(f.read())
f.close()

url = "https://accounts.sdslabs.co.in/search"
counter = 0
f = open("raw_detail_with_new_line.json", "a")
for name in names:
	data = {"query": name}

	try:
		p = r.get(url, params=data)
	except Exception, e:
		print "break at %d" % counter
		raise e

	if (p.status_code != 200):
		print "break at %d" % counter
		break

	f.write(p.text+"\n")
	counter += 1

print "I'm done at %d" % counter
f.close()
